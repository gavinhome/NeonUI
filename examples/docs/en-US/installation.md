## Installation

### npm

Installing with npm is recommended and it works seamlessly with [webpack](https://webpack.js.org/).

```shell
npm i neon-ui -S
```

### CDN

Get the latest version from [unpkg.com/neon-ui](https://unpkg.com/neon-ui/) , and import JavaScript and CSS file in your page.

```html
<!-- import CSS -->
<link rel="stylesheet" href="https://unpkg.com/neon-ui/lib/theme-chalk/index.css">
<!-- import JavaScript -->
<script src="https://unpkg.com/neon-ui/lib/index.js"></script>
```

:::tip
We recommend our users to lock Neon's version when using CDN. Please refer to [unpkg.com](https://unpkg.com) for more information.
:::

### Hello world

If you are using CDN, a hello-world page is easy with Neon. [Online Demo](https://jsfiddle.net/hzfpyvg6/14/)

<iframe width="100%" height="600" src="//jsfiddle.net/hzfpyvg6/1213/embedded/html,result/" allowpaymentrequest allowfullscreen="allowfullscreen" frameborder="0"></iframe>

If you are using npm and wish to apply webpack, please continue to the next page: [Quick Start](/#/en-US/component/quickstart).
