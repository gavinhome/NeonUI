## Installation

### NPM

Installer Neon via npm est recommandé, il fonctionne parfaitement avec [webpack](https://webpack.js.org/).

```shell
npm i neon-ui -S
```

### CDN

Obtenez la dernière version via [unpkg.com/neon-ui](https://unpkg.com/neon-ui/), et importez le JavaScript et le CSS dans votre page.

```html
<!-- import du CSS -->
<link rel="stylesheet" href="https://unpkg.com/neon-ui/lib/theme-chalk/index.css">
<!-- import du JavaScript -->
<script src="https://unpkg.com/neon-ui/lib/index.js"></script>
```

:::tip
Il est recommandé de fixer la version d'Neon lors de l'utilisation du CDN. Référez-vous à  [unpkg.com](https://unpkg.com) pour plus d'informations.
:::

### Hello world

Si vous utilisez un CDN, une page hello-world peut être obtenue facilement avec Neon ([démo en ligne](https://jsfiddle.net/hzfpyvg6/14/)).

<iframe width="100%" height="600" src="//jsfiddle.net/hzfpyvg6/1213/embedded/html,result/" allowpaymentrequest allowfullscreen="allowfullscreen" frameborder="0"></iframe>

Si vous utilisez npm et souhaitez ajouter webpack, continuez sur la page suivante: [Quick Start](/#/fr-FR/component/quickstart).
