import Neon from 'main/index.js';
import { post, get } from './ajax';

const { version } = Neon;

const hostList = {
  local: 'http://localhost:3008/',
  alpha: 'https://ssr.alpha.elenet.me/neon-theme-server/',
  production: 'https://ssr.elenet.me/neon-theme-server/'
};

const host = hostList[process.env.FAAS_ENV] || hostList.production;

export const getVars = () => {
  return get(`${host}getVariable?version=${version}`);
};

export const updateVars = (data, cb) => {
  return post(`${host}updateVariable?version=${version}`, data, cb);
};
