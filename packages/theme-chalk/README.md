# neon-theme-chalk
> element component chalk theme.


## Installation
```shell
npm i neon-theme-chalk -S
```

## Usage

Use Sass import
```css
@import 'neon-theme-chalk';
```

Or Use webpack
```javascript
import 'neon-theme-chalk';
```

Or
```html
<link rel="stylesheet" href="path/to/node_modules/neon-theme-chalk/lib/index.css">
```

##  Import on demand
```javascript
import 'neon-theme-chalk/lib/input.css';
import 'neon-theme-chalk/lib/select.css';

// ...
```
