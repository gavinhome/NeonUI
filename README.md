<p align="center">
  <img src="https://gitee.com/gavinhome/NeonUI/tree/dev/neon_logo.svg">
</p>

> A Vue.js 2.0 UI Toolkit for Web.

## Links
- [awesome-element](https://github.com/ElementUI/awesome-element)
- [FAQ](./FAQ.md)
<!-- - [Customize theme](http://element.eleme.io/#/en-US/component/custom-theme)
- [Preview and generate theme online](https://neonui.github.io/theme-chalk-preview)
- [Neon for React](https://github.com/neonfe/neon-react)
- [Neon for Angular](https://github.com/NeonFE/neon-angular)
- [Atom helper](https://github.com/NeonFE/neon-helper)
- [Visual Studio Code helper](https://github.com/NeonFE/vscode-neon-helper) -->
<!-- - Starter kit
  - [neon-starter](https://github.com/NeonUI/neon-starter)
  - [neon-in-laravel-starter](https://github.com/NeonUI/neon-in-laravel-starter) -->
<!-- - [Design resources](https://github.com/NeonUI/Resources) -->
<!-- - Gitter
  - [International users](https://gitter.im/neon-en/Lobby)
  - [Chinese users](https://gitter.im/NeonFE/element) -->

## Install
```shell
npm install neon-ui -S
```

## Quick Start
``` javascript
import Vue from 'vue'
import Neon from 'neon-ui'

Vue.use(Neon)

// or
import {
  Select,
  Button
  // ...
} from 'neon-ui'

Vue.component(Select.name, Select)
Vue.component(Button.name, Button)
```
For more information, please refer to [Quick Start](http://gavinhome.gitee.io/neonui/#/en-US/component/quickstart) in our documentation.

## Browser Support
Modern browsers and Internet Explorer 10+.

## Development
Skip this part if you just want to use Neon.

For those who are interested in contributing to Neon, please refer to our contributing guide ([中文](https://github.com/NeonFE/neon/blob/master/.github/CONTRIBUTING.zh-CN.md) | [English](https://github.com/NeonFE/neon/blob/master/.github/CONTRIBUTING.en-US.md) | [Español](https://github.com/NeonFE/neon/blob/master/.github/CONTRIBUTING.es.md) | [Français](https://github.com/NeonFE/neon/blob/master/.github/CONTRIBUTING.fr-FR.md)) to see how to run this project.

## Changelog
Detailed changes for each release are documented in the [release notes](https://github.com/NeonFE/neon/releases).

## FAQ
We have collected some [frequently asked questions](https://github.com/NeonFE/neon/blob/master/FAQ.md). Before reporting an issue, please search if the FAQ has the answer to your problem.

## Contribution
Please make sure to read the contributing guide ([中文](https://github.com/NeonFE/neon/blob/master/.github/CONTRIBUTING.zh-CN.md) | [English](https://github.com/NeonFE/neon/blob/master/.github/CONTRIBUTING.en-US.md) | [Español](https://github.com/NeonFE/neon/blob/master/.github/CONTRIBUTING.es.md) | [Français](https://github.com/NeonFE/neon/blob/master/.github/CONTRIBUTING.fr-FR.md)) before making a pull request.

[![Let's fund issues in this repository](https://issuehunt.io/static/embed/issuehunt-button-v1.svg)](https://issuehunt.io/repos/67274736)

## Special Thanks

## Donation
If you find Neon useful, you can buy us a cup of coffee

<!-- <img width="650" src="" alt="donation"> -->

## Backers

Support us with a monthly donation and help us continue our activities. [[Become a backer](https://opencollective.com/element#backer)]

## Sponsors

Become a sponsor and get your logo on our README on Github with a link to your site. [[Become a sponsor](https://opencollective.com/element#sponsor)]

## Join Discusion Group

Scan the QR code using [Dingtalk App](https://www.dingtalk.com/) to join in discusion group :

<!-- <img alt="Join Discusion Group" src="" width="300"> -->

## LICENSE
[MIT](LICENSE)
